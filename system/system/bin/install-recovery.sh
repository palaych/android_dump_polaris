#!/system/bin/sh
if ! applypatch --check EMMC:/dev/block/bootdevice/by-name/recovery:58520910:d1fcdba04233328f82c82b6777fb730b28bf1ee5; then
  applypatch --bonus /system/etc/recovery-resource.dat \
          --patch /system/recovery-from-boot.p \
          --source EMMC:/dev/block/bootdevice/by-name/boot:45053258:c9c9da1de1de5fe01fa3d481937e376dd64a6933 \
          --target EMMC:/dev/block/bootdevice/by-name/recovery:58520910:d1fcdba04233328f82c82b6777fb730b28bf1ee5 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
